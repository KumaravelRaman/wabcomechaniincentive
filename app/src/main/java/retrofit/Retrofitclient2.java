package retrofit;

import java.util.concurrent.TimeUnit;

import api.APIService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vibin on 22-Nov-17.
 */

public class Retrofitclient2 {
    //private static final String ROOT_URL = "http://125.17.182.100/api/Values/";
    private static final String ROOT_URL = "http://genuinecheck.wabcoindia-am.com/";
    /**
     * Get Retrofit Instance
     */
    private static OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build();


    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static APIService getApiService() {
        return getRetrofitInstance().create(APIService.class);
    }
}
