package com.brainmagic.wabcoincentive;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import logout.Logout;
import toaster.Toasts;

public class MainActivity extends AppCompatActivity {
        private RelativeLayout barcodeLayout,scanhistoryLayout;
        private Toasts toasts;
        private ImageView signOut;
        private String userType,nameType,totalVal;
        private TextView NameMenu;
        private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toasts = new Toasts(MainActivity.this);
        barcodeLayout = (RelativeLayout) findViewById(R.id.barcodescan_relativelayout);
        NameMenu = (TextView)findViewById(R.id.homename);
        signOut = (ImageView)findViewById(R.id.signout);
        nameType = getIntent().getStringExtra("name");
        scanhistoryLayout = (RelativeLayout) findViewById(R.id.scanhistory_relativelayout);
        userType = getIntent().getStringExtra("usertype");
        totalVal = nameType + "("+userType+")";

  //      NameMenu.setText(userType);
        barcodeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, BarcodeSelection.class);
                i.putExtra("usertype",userType);
                startActivity(i);
               // toasts.ShowSuccessToast("Move on Next Activity");
            }
        });

        scanhistoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ScanHistory.class);

                startActivity(i);
               // toasts.ShowErrorToast("Under Development");
            }
        });

        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Logout(MainActivity.this).log_out();

            }
        });

        final ImageView sidemenu = (ImageView) findViewById(R.id.menu);

        sidemenu.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(MainActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.barcode:
                                startActivity(new Intent(MainActivity.this, BarcodeSelection.class));
                                break;
                            case R.id.viewscan:
                                startActivity(new Intent(MainActivity.this, ScanHistory.class));
                                break;
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.main);
                pop.show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
    }
}
