package com.brainmagic.wabcoincentive;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.zxing.client.android.Intents;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import alertbox.Alertbox;

public class BarcodeSelection extends AppCompatActivity {
  //  final int REQ_CODE = 12;
    //qr code scanner objectwithoutScan
    private IntentIntegrator qrScan;
   // private Alertbox box = new Alertbox(BarcodeSelection.this);
    private RadioButton mWithScan,mWithoutScan;
    private RadioGroup mScan_group;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String mSixDigit,userType;
    private ImageView backBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_selection);
        qrScan = new IntentIntegrator(this);
        sharedPreferences = this.getSharedPreferences("registration", Context.MODE_PRIVATE);
        userType =  sharedPreferences.getString("usertype", " ");
        mScan_group = (RadioGroup) findViewById(R.id.scan_group);
        mWithScan = (RadioButton) findViewById(R.id.withScan);
        backBtn = (ImageView)findViewById(R.id.backbtn);
        mWithoutScan = (RadioButton) findViewById(R.id.withoutScan);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void scan(View view) {
        int selectedId = mScan_group.getCheckedRadioButtonId();
        // find which radioButton is checked by id
        if(selectedId == mWithScan.getId()) {
            qrScan.initiateScan();
            qrScan.setBeepEnabled(true);
            qrScan.setOrientationLocked(false);
            qrScan.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        }  else if(selectedId == mWithoutScan.getId()) {
            new MaterialDialog.Builder(BarcodeSelection.this).title("WABCO Geniune")
                    .content("Enter unique ID without m")
                    .autoDismiss(false)
                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT)
                    .input("Enter full unique ID", "", new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(MaterialDialog dialog, CharSequence input) {
                            // Do something
                            mSixDigit = input.toString();
                        }
                    }).positiveText("Search").negativeText("Cancel")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (dialog.getInputEditText().getText().length() >=3 ) {
                                mSixDigit = dialog.getInputEditText().getText().toString();
                                // mSixDigit = "350024046-C14F18-1-208";
                                dialog.dismiss();
                                Intent a = new Intent(BarcodeSelection.this, Serial_List_Activity.class);
                                a.putExtra("mSixDigit", mSixDigit);
                                a.putExtra("usertype",userType);
                                startActivity(a);
                            } else {
                                StyleableToast st = new StyleableToast(BarcodeSelection.this,
                                        "Please Enter full unique ID without m", Toast.LENGTH_SHORT);
                                st.setBackgroundColor(getResources().getColor(R.color.red));
                                st.setTextColor(Color.WHITE);
                                st.setMaxAlpha();
                                st.show();
                            }
                        }
                    }).onNegative(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    dialog.dismiss();
                }
            }).show();

        }
        else
        {
            StyleableToast st = new StyleableToast(BarcodeSelection.this,
                    "Select any scanning option!", Toast.LENGTH_SHORT);
            st.setBackgroundColor(getResources().getColor(R.color.red));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
        }

      /*  Intent a = new Intent(BarcodeSelection.this, Sacn.class);
        a.putExtra("Serial id", "350024072-A10D17-1-1116");
        startActivity(a);
*/
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Scanning failed try again!", Toast.LENGTH_LONG).show();
            }
            else if(result.getContents().toString().split("-").length!=4)
            {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.custom_genuine_layout, null);
                final TextView Msg_txtview = alertLayout.findViewById(R.id.al_msg);
                final TextView Sl_txtview = alertLayout.findViewById(R.id.sl_no);

                Msg_txtview.setText("Scanning failed. \nFound incorrect serial number!\nDo you want scan again ?");
                Sl_txtview.setText(result.getContents().toString());
                Sl_txtview.setTextColor(Color.RED);
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(BarcodeSelection.this);
                alertDialog.setTitle("WABCO Geniune");
                alertDialog.setCancelable(false);
                alertDialog.setView(alertLayout);
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        qrScan.initiateScan();
                        qrScan.setBeepEnabled(true);
                        qrScan.setOrientationLocked(false);
                        qrScan.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
            else {
                //if qr contains data
               /* if(userType.equals("Retailer")) {
                    Intent a = new Intent(BarcodeSelection.this, RetailerResult.class);
                    a.putExtra("Serial id", result.getContents().toString());
                    startActivity(a);
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }else{
                    Intent a = new Intent(BarcodeSelection.this, MechanicResult.class);
                    a.putExtra("Serial id", result.getContents().toString());
                    startActivity(a);
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }*/

                Intent a = new Intent(BarcodeSelection.this, Sacn.class);
                a.putExtra("Serial id", result.getContents().toString());
                startActivity(a);
                Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
