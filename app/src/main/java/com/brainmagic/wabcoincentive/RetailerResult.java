package com.brainmagic.wabcoincentive;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import toaster.Toasts;

public class RetailerResult extends AppCompatActivity {
    private EditText incentiveAmountrelmecEdit,sourceofPurchaseEdit,stateEdit,cityEdit;
    private String str_Incentiveamount,str_SourceofPurchase,str_State,str_City;
    private Toasts toasts;
    private Button okBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retailer_result);

        toasts = new Toasts(RetailerResult.this);
        incentiveAmountrelmecEdit = (EditText)findViewById(R.id.incentiveamountedit);
        sourceofPurchaseEdit = (EditText)findViewById(R.id.sourceofpurchaseedit);
        stateEdit = (EditText)findViewById(R.id.stateedit);
        cityEdit = (EditText)findViewById(R.id.cityedit);
        okBtn = (Button)findViewById(R.id.retailerokbtn);
        str_Incentiveamount = getIntent().getStringExtra("incentiveamount");
        str_SourceofPurchase = getIntent().getStringExtra("sourceofpurchase");
        str_State = getIntent().getStringExtra("state");
        str_City = getIntent().getStringExtra("city");
        incentiveAmountrelmecEdit.setText("Rs"+str_Incentiveamount);
        sourceofPurchaseEdit.setText(str_SourceofPurchase);
        stateEdit.setText(str_State);
        cityEdit.setText(str_City);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RetailerResult.this,MainActivity.class);
                startActivity(i);
              //  toasts.ShowSuccessToast("Under Development... Move on Home-Screen");
            }
        });
    }
}
