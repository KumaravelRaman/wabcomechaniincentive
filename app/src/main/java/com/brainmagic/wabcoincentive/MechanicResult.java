package com.brainmagic.wabcoincentive;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import toaster.Toasts;

public class MechanicResult extends AppCompatActivity {
    private EditText incentiveAmountEdit,dateofScanEdit,dealerNameEdit,dealerCityEdit,dealerStateEdit;
    private String str_Incentiveamount,str_Dateofscan,str_Dealername,str_dealercity,str_dealerstate,str_alert;
    private Toasts toasts;
    private Button mechanicOkBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mechanic_result);
        toasts = new Toasts(MechanicResult.this);
        incentiveAmountEdit = (EditText)findViewById(R.id.incentiveamountedit);
        dateofScanEdit = (EditText)findViewById(R.id.dateofscanedit);
        dealerNameEdit = (EditText)findViewById(R.id.dealernameedit);
        dealerCityEdit = (EditText)findViewById(R.id.dealercityedit);
        dealerStateEdit = (EditText)findViewById(R.id.dealerstateedit);
        str_Incentiveamount = getIntent().getStringExtra("mechincentiveamount");
        str_Dateofscan = getIntent().getStringExtra("dateofscan");
        str_Dealername = getIntent().getStringExtra("dealername");
        str_dealercity = getIntent().getStringExtra("dealercity");
        str_dealerstate = getIntent().getStringExtra("dealerstate");
        incentiveAmountEdit.setText("Rs:" +str_Incentiveamount);
        dateofScanEdit.setText(str_Dateofscan);
        dealerNameEdit.setText(str_Dealername);
        dealerCityEdit.setText(str_dealercity);
        dealerStateEdit.setText(str_dealerstate);
        str_alert = getIntent().getStringExtra("alert");

        if(str_alert.equals("alert")){
            final AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(MechanicResult.this).create();
            LayoutInflater inflater = ((MechanicResult.this)).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.incentivepopup, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Button Back = (Button) dialogView.findViewById(R.id.okbtn);


            Back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //progressDialog.dismiss();
                    alertDialog.dismiss();

                }
            });
            alertDialog.show();
        }else {

        }
        mechanicOkBtn = (Button)findViewById(R.id.mechanicokbtn);
        mechanicOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MechanicResult.this,MainActivity.class);
                startActivity(i);
               // toasts.ShowSuccessToast("Under Development ... Move on Home-Screen");
            }
        });
    }
}
