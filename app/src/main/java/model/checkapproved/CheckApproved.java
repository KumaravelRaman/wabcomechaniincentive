
package model.checkapproved;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CheckApproved {

    @SerializedName("data")
    private CheckApprovedData mData;
    @SerializedName("result")
    private String mResult;

    public CheckApprovedData getData() {
        return mData;
    }

    public void setData(CheckApprovedData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
